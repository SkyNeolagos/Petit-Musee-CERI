package fr.skyneolagos.petitmusee.service.worker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.skyneolagos.petitmusee.artefact.Artefact;
import fr.skyneolagos.petitmusee.artefact.ArtefactComparator;
import fr.skyneolagos.petitmusee.activity.MainActivity;
import fr.skyneolagos.petitmusee.json.JSONResponseHandlerArtefact;

@SuppressLint("StaticFieldLeak")
public class WorkerArtefact extends AsyncTask<URL,Integer, ArrayList<Artefact>> {
    private List<Artefact> artefactList;
    private Context context;

    public WorkerArtefact(List<Artefact> artefactList, Context context) {
        this.artefactList = artefactList;
        this.context=context;
    }
    @Override
    protected ArrayList<Artefact> doInBackground(URL... urls) {
        InputStream inputStream;
        JSONResponseHandlerArtefact jsonResponseHandlerArtefact=new JSONResponseHandlerArtefact(this.artefactList);
        HttpURLConnection httpURLConnection;

        try {
            httpURLConnection=(HttpURLConnection) urls[0].openConnection();
            inputStream=new BufferedInputStream(httpURLConnection.getInputStream());
            try{
                jsonResponseHandlerArtefact.readJsonStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (ArrayList<Artefact>) this.artefactList;
    }

    @Override
    protected void onPostExecute(ArrayList<Artefact> artefacts) {
        super.onPostExecute(artefacts);
        if(context instanceof MainActivity){
            ((MainActivity) context).updateRecycler();
            ((MainActivity) context).swipeRefreshLayout.setRefreshing(false);
            Collections.sort(artefactList,new ArtefactComparator(ArtefactComparator.CompareOption.NOM));
        }
    }
}
