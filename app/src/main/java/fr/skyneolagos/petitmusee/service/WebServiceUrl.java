package fr.skyneolagos.petitmusee.service;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";
    private static final String SEARCH_IDS = "ids";
    private static final String SEARCH_CATEGORIES = "categories";
    private static final String SEARCH_CATALOG = "catalog";
    private static final String SEARCH_ITEMS = "items";
    private static final String SEARCH_IMAGE = "images";
    private static final String SEARCH_DEMOS = "demos";
    private static final String SEARCH_THUMBNAIL = "thumbnail";
    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    // Get ids
    // https://demo-lia.univ-avignon.fr/cerimuseum/ids
    public static URL buildSearchAllArtefact() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_IDS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get categories
    // https://demo-lia.univ-avignon.fr/cerimuseum/categories
    public static URL buildSearchCategories() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATEGORIES);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get catalog
    // https://demo-lia.univ-avignon.fr/cerimuseum/catalog
    public static URL buildSearchCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }


    // Get thumbnail by idAtefact
    //https://demo-lia.univ-avignon.fr/cerimuseum/items/idAtefact/thumbnail
    public static URL buildGetThumbnailById(String idAtefact) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS).appendPath(idAtefact).appendPath(SEARCH_THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get artefact by idAtefact
    //https://demo-lia.univ-avignon.fr/cerimuseum/items/idAtefact
    public static URL buildGetArtefactById(String idAtefact) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS).appendPath(idAtefact);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get images by idImage and idArtefact
    //https://demo-lia.univ-avignon.fr/cerimuseum/items/idArtefact/images/idImage
    public static URL buildGetImageByidArtefactAndidImage(String idArtefact , String idImage) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS).appendPath(idArtefact).appendPath(SEARCH_IMAGE).appendPath(idImage);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get demos
    //https://demo-lia.univ-avignon.fr/cerimuseum/demos
    public static URL buildGetDemos() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_DEMOS);
        URL url = new URL(builder.build().toString());
        return url;
    }
}
