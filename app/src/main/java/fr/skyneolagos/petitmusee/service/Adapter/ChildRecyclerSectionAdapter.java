package fr.skyneolagos.petitmusee.service.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fr.skyneolagos.petitmusee.artefact.Artefact;
import fr.skyneolagos.petitmusee.R;

public class ChildRecyclerSectionAdapter extends RecyclerView.Adapter<ChildRecyclerSectionAdapter.ViewHolderChild> {
    private List<Artefact> artefactList;
    private static ClickListenerChild clickListenerSection;

    public ChildRecyclerSectionAdapter(List<Artefact> artefactList) {
        this.artefactList = artefactList;
    }

    @NonNull
    @Override
    public ViewHolderChild onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.item_artefact,parent,false);
        return new ViewHolderChild(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderChild holder, int position) {
        holder.bind(artefactList.get(position));
    }

    @Override
    public int getItemCount() {
        return artefactList.size();
    }

    public Artefact getArtefactAtPosition(int position){
        return artefactList.get(position);
    }

    public interface ClickListenerChild{
        void onItemClickChild(Artefact artefact, View v);
    }
    void setClickListenerSection(ClickListenerChild clickListenerSection){
        ChildRecyclerSectionAdapter.clickListenerSection =clickListenerSection;
    }

    class ViewHolderChild extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView artefactNameHolder;
        private TextView artefactBrandHolder;
        private ImageView artefactImageHolder;
        private TextView artefactCategoryHolder;

        ViewHolderChild(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            artefactNameHolder = itemView.findViewById(R.id.artefactNameItem);
            artefactBrandHolder = itemView.findViewById(R.id.artefactBrandItem);
            artefactImageHolder = itemView.findViewById(R.id.artefactImageItem);
            artefactCategoryHolder = itemView.findViewById(R.id.artefactCategorieItem);
        }

        void bind(Artefact item) {
            artefactNameHolder.setText(item.getName());
            artefactBrandHolder.setText(item.getBrand());
            artefactCategoryHolder.setText(item.getCategoriesToString());
            if (item.getThumbnail() != null && !item.getThumbnail().isEmpty()) {
                Picasso.with(itemView.getContext()).load(item.getThumbnail()).into(artefactImageHolder);
            } else {
                artefactImageHolder.setImageResource(R.mipmap.ic_launcher);
            }

        }

        @Override
        public void onClick(View v) {
            clickListenerSection.onItemClickChild(getArtefactAtPosition(getAdapterPosition()),v);
        }
    }
}
