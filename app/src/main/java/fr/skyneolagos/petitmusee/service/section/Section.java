package fr.skyneolagos.petitmusee.service.section;

import java.util.ArrayList;
import java.util.List;

import fr.skyneolagos.petitmusee.artefact.Artefact;

public class Section {
    private String sectionName;
    private List<Artefact> artefactListSection=new ArrayList<>();

    public Section(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public List<Artefact> getArtefactListSection() {
        return artefactListSection;
    }


    public void populateListSection(List<Artefact> artefactListAll){
        for (int i = 0; i < artefactListAll.size(); i++) {
            for (int j = 0; j < artefactListAll.get(i).getCategories().size(); j++) {
                if(artefactListAll.get(i).getCategories().get(j).equals(sectionName)){
                    artefactListSection.add(artefactListAll.get(i));
                }
            }
        }
    }
    public Artefact getArtefactAtPosition(int position){
        return artefactListSection.get(position);
    }
}
