package fr.skyneolagos.petitmusee.service.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.skyneolagos.petitmusee.artefact.Artefact;
import fr.skyneolagos.petitmusee.R;
import fr.skyneolagos.petitmusee.activity.ArtefactDetailActivity;
import fr.skyneolagos.petitmusee.service.section.Section;

public class ArtefactSectionAdapter extends RecyclerView.Adapter<ArtefactSectionAdapter.ViewHolder> {
    private List<Section> sectionList;
    private Context context;
    public ArtefactSectionAdapter(List<Section> sectionList,Context context) {
        this.sectionList = sectionList;
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        View view=layoutInflater.inflate(R.layout.section_recycler,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtefactSectionAdapter.ViewHolder holder, int position) {
        holder.bind(this.sectionList.get(position),context);
    }

    @Override
    public int getItemCount() {
        return sectionList.size();
    }

   static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView sectionName;
        private RecyclerView childRecyclerView;
        private ChildRecyclerSectionAdapter childRecyclerSectionAdapter;

       public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sectionName=itemView.findViewById(R.id.sectionName);
            childRecyclerView=itemView.findViewById(R.id.recyclerViewChild);
        }
        private void bind(Section section,Context context) {
            sectionName.setText(section.getSectionName());
            childRecyclerSectionAdapter=new ChildRecyclerSectionAdapter(section.getArtefactListSection());
            childRecyclerView.setAdapter(childRecyclerSectionAdapter);
            childRecyclerSectionAdapter.setClickListenerSection((artefact, v) -> {
                    Intent intent= new Intent(context, ArtefactDetailActivity.class);
                    intent.putExtra(Artefact.TAG, artefact);
                    context.startActivity(intent);
            });

        }
    }
}
