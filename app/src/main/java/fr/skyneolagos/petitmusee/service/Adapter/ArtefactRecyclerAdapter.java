package fr.skyneolagos.petitmusee.service.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import fr.skyneolagos.petitmusee.artefact.Artefact;
import fr.skyneolagos.petitmusee.R;

public class ArtefactRecyclerAdapter extends RecyclerView.Adapter<ArtefactRecyclerAdapter.ArtefactViewHolder> implements Filterable {
    private List<Artefact> artefactListAdapter;
    private List<Artefact> artefactListAdapterAll;
    private Context context;
    private static ClickListener clickListener;

    public ArtefactRecyclerAdapter(Context context,List<Artefact> artefactListAdapter) {
        this.context=context;
        this.artefactListAdapter = artefactListAdapter;
        this.artefactListAdapterAll=artefactListAdapter;
    }

    @NonNull
    @Override
    public ArtefactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_artefact, parent, false);
        return new ArtefactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtefactRecyclerAdapter.ArtefactViewHolder holder, int position) {
        holder.bind(artefactListAdapter.get(position));
    }


    @Override
    public int getItemCount() {
        return artefactListAdapter.size();
    }

    public Artefact getArtefactAtPosition(int position){
        return artefactListAdapter.get(position);
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    artefactListAdapter = artefactListAdapterAll;
                } else {
                    List<Artefact> filteredList = new ArrayList<>();
                    for (Artefact artefact : artefactListAdapterAll) {
                        //On regarde le nom
                        if (artefact.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(artefact);
                        }else {//Sinon on regarde les categories
                            for (int i = 0; i < artefact.getCategories().size(); i++) {
                                if(artefact.getCategories().get(i).contains(charString.toLowerCase())){
                                    filteredList.add(artefact);
                                }
                            }
                        }
                    }
                    artefactListAdapter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = artefactListAdapter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                    artefactListAdapter = (List<Artefact>) results.values;
                    notifyDataSetChanged();
            }
        };
    }

    public interface ClickListener{
        void onItemClick(int position,View v);
    }
    public void setOnItemClicklistener(ClickListener clicklistener){
        clickListener=clicklistener;
    }

    static class ArtefactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView artefactNameHolder;
        private TextView artefactBrandHolder;
        private ImageView artefactImageHolder;
        private TextView artefactCategoryHolder;

        ArtefactViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            artefactNameHolder = itemView.findViewById(R.id.artefactNameItem);
            artefactBrandHolder = itemView.findViewById(R.id.artefactBrandItem);
            artefactImageHolder = itemView.findViewById(R.id.artefactImageItem);
            artefactCategoryHolder = itemView.findViewById(R.id.artefactCategorieItem);
        }


        void bind(Artefact item) {
            artefactNameHolder.setText(item.getName());
            artefactBrandHolder.setText(item.getBrand());
            artefactCategoryHolder.setText(item.getCategoriesToString());
            if (item.getThumbnail() != null && !item.getThumbnail().isEmpty()) {
                Picasso.with(itemView.getContext()).load(item.getThumbnail()).into(artefactImageHolder);
            } else {
                artefactImageHolder.setImageResource(R.mipmap.ic_launcher);
            }

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(),v);
        }
    }
}
