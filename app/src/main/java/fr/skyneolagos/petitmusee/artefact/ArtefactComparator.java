package fr.skyneolagos.petitmusee.artefact;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class ArtefactComparator implements Comparator<Artefact> {
    private CompareOption optionComparator;
    private static Collator COLLATOR = Collator.getInstance(Locale.FRENCH);

    public ArtefactComparator(CompareOption optionComparator) {
        this.optionComparator = optionComparator;
    }

    @Override
    public int compare(Artefact o1, Artefact o2) {
        if(optionComparator==CompareOption.NOM){
            return COLLATOR.compare(o1.getName(),o2.getName());
        }
        else{
            int result=o1.getYear()-o2.getYear();
            if(result==0){//Annee identique
                return COLLATOR.compare(o1.getName(),o2.getName());//On trie alors par l'annee et par le nom
            }
            else {
                return result;
            }
        }
    }
    public enum CompareOption{
        NOM,YEAR;
    }
}
