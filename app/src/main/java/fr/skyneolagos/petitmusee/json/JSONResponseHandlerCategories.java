package fr.skyneolagos.petitmusee.json;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import fr.skyneolagos.petitmusee.service.section.Section;

public class JSONResponseHandlerCategories {
    private static final String TAG = JSONResponseHandlerCategories.class.getSimpleName();
    private List<Section> sectionList;

    public JSONResponseHandlerCategories(List<Section> sectionList) {
        this.sectionList = sectionList;
    }

    /**
     * @param response done by the Web service
     * @return A List of Section with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCategories(reader);
        } finally {
            reader.close();
        }
    }
    private void readCategories(JsonReader reader) throws IOException {
        reader.beginArray();
        int indice=0;
        while(reader.hasNext()){
            Section section=new Section(reader.nextString());
            sectionList.add(indice,section);
            indice++;
        }
        reader.endArray();
    }
}
