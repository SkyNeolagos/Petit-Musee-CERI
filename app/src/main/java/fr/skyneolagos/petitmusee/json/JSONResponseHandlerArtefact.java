package fr.skyneolagos.petitmusee.json;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import fr.skyneolagos.petitmusee.artefact.Artefact;
import fr.skyneolagos.petitmusee.service.WebServiceUrl;

public class JSONResponseHandlerArtefact {
    private static final String TAG = JSONResponseHandlerArtefact.class.getSimpleName();
    private List<Artefact> artefactList;

    public JSONResponseHandlerArtefact(List<Artefact> artefactList) {
        this.artefactList = artefactList;
    }


    /**
     * @param response done by the Web service
     * @return A artefactList with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCatalog(reader);
        } finally {
            reader.close();
        }

    }
    private void readCatalog(JsonReader reader) throws IOException {
        reader.beginObject();
        while(reader.hasNext()){
            Artefact artefact=new Artefact(reader.nextName());
            reader.beginObject();
            while (reader.hasNext()){
                String name=reader.nextName();
                switch (name){
                    //TODO technicalDetails ,pictures
                    case "name":
                        artefact.setName(reader.nextString());
                        break;
                    case "categories":
                        reader.beginArray();
                        List<String> tableauCategorie=new ArrayList<String>();
                        while (reader.hasNext()){
                            tableauCategorie.add(reader.nextString());
                        }
                        reader.endArray();
                        artefact.setCategories(tableauCategorie);
                        break;

                    case "description":
                        artefact.setDescription(reader.nextString());
                        break;
                    case "timeFrame":
                        reader.beginArray();
                        List<Integer> tableauTimeFrame=new ArrayList<Integer>();
                        while (reader.hasNext()){
                            tableauTimeFrame.add(reader.nextInt());
                        }
                        reader.endArray();
                        artefact.setTimeFrame(tableauTimeFrame);
                        break;

                    case "year":
                        artefact.setYear(reader.nextInt());
                        break;
                    case "brand":
                        artefact.setBrand(reader.nextString());
                        break;
                    case "working":
                        if(reader.nextBoolean()){
                            artefact.setWorking(1);
                        }
                        break;
                    case "technicalDetails":
                        reader.beginArray();
                        while (reader.hasNext()){
                            reader.skipValue();
                        }
                        reader.endArray();
                        break;

                    case "pictures":
                        reader.beginObject();
                        while (reader.hasNext()){
                            reader.skipValue();
                        }
                        reader.endObject();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            artefact.setThumbnail(WebServiceUrl.buildGetThumbnailById(artefact.getId()).toString());
            artefactList.add(artefact);
            reader.endObject();
        }
    }
}
