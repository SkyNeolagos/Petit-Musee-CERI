package fr.skyneolagos.petitmusee.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.skyneolagos.petitmusee.artefact.Artefact;
import fr.skyneolagos.petitmusee.artefact.ArtefactComparator;
import fr.skyneolagos.petitmusee.R;
import fr.skyneolagos.petitmusee.service.Adapter.ArtefactRecyclerAdapter;
import fr.skyneolagos.petitmusee.service.section.Section;
import fr.skyneolagos.petitmusee.service.WebServiceUrl;
import fr.skyneolagos.petitmusee.service.worker.WorkerArtefact;
import fr.skyneolagos.petitmusee.service.worker.WorkerCategories;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    public static List<Artefact> artefactList= new ArrayList<>();//Liste des artefacts
    public static List<Section> sectionList=new ArrayList<>();//Liste des sections

    private RecyclerView recyclerView;
    private ArtefactRecyclerAdapter artefactRecyclerAdapter;
    public SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        swipeRefreshLayout= findViewById(R.id.swipeRefresh);
        recyclerView=findViewById(R.id.artefactListRecyclerView);

        //On lance le WorkerArtefact si la liste d'artefact est vide
        if(artefactList.size()==0){
            WorkerArtefact workerArtefact =new WorkerArtefact(artefactList,MainActivity.this);
            try {
                workerArtefact.execute(WebServiceUrl.buildSearchCatalog());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        //On initialise les noms des sections
        sectionList.clear();
        initSection();

        //On set notre adapter sur notre RecyclerView
        artefactRecyclerAdapter=new ArtefactRecyclerAdapter(this,artefactList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(artefactRecyclerAdapter);


        //Implementation de la fonction permettant de lancer l'activité ArtefactDetailActivity sur l'artefact cliqué
        artefactRecyclerAdapter.setOnItemClicklistener((position, v) -> {
            Intent intent= new Intent(MainActivity.this,ArtefactDetailActivity.class);
            intent.putExtra(Artefact.TAG, artefactRecyclerAdapter.getArtefactAtPosition(position));
            startActivity(intent);
        });

        //Implmentation d'un refreshListener sur notre vue
        swipeRefreshLayout.setOnRefreshListener(()->{
            //On lance le WorkerArtefact si la liste d'artefact est vide
            if(artefactList.size()==0){
                initSection();
                WorkerArtefact workerArtefact =new WorkerArtefact(artefactList,MainActivity.this);
                try {
                    workerArtefact.execute(WebServiceUrl.buildSearchCatalog());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            }
            else{
                swipeRefreshLayout.setRefreshing(false);
            }
            sectionList.clear();
            initSection();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem=menu.findItem(R.id.app_bar_search);
        SearchView searchView= (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                artefactRecyclerAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                artefactRecyclerAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.app_bar_search){
            return true;
        }
        if (id == R.id.action_settings) {
            Intent intent= new Intent(MainActivity.this,SectionActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id==R.id.sortName){
            item.setChecked(!item.isChecked());
            Collections.sort(artefactList,new ArtefactComparator(ArtefactComparator.CompareOption.NOM));
            updateRecycler();
            return true;
        }
        else if(id==R.id.sortYear){
            item.setChecked(!item.isChecked());
            Collections.sort(artefactList,new ArtefactComparator(ArtefactComparator.CompareOption.YEAR));
            updateRecycler();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void updateRecycler(){
        recyclerView.setAdapter(artefactRecyclerAdapter);
    }
    public void initSection(){
        WorkerCategories workerCategories =new WorkerCategories(artefactList, sectionList, MainActivity.this);
        try {
            workerCategories.execute(WebServiceUrl.buildSearchCategories());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
