package fr.skyneolagos.petitmusee.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import fr.skyneolagos.petitmusee.artefact.Artefact;
import fr.skyneolagos.petitmusee.R;

public class ArtefactDetailActivity extends AppCompatActivity {
    private static final String TAG = ArtefactDetailActivity.class.getSimpleName();
    private Artefact artefact;

    private TextView textArtefactName, textArtefactCategorie, textArtefactDescription, textArtefactBrand, textArtefactWorking, textArtefactYear ;
    private ImageView imageArtefactThumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artefact_detail);
        artefact = getIntent().getParcelableExtra(Artefact.TAG);

        textArtefactName=findViewById(R.id.artefactNameDetail);
        textArtefactCategorie=findViewById(R.id.artefactCategoriesDetail);
        textArtefactBrand=findViewById(R.id.brandView);
        textArtefactDescription=findViewById(R.id.descriptionView);
        textArtefactWorking=findViewById(R.id.workingview);
        textArtefactYear=findViewById(R.id.yearView);
        imageArtefactThumbnail=findViewById(R.id.artefactThumbnailDetail);

        updateView();
    }

    @SuppressLint("SetTextI18n")
    void updateView(){
        textArtefactName.setText(artefact.getName());
        textArtefactCategorie.setText(artefact.getCategoriesToString());
        if(artefact.getBrand()!=null){
            textArtefactBrand.setText(artefact.getBrand());
        }
        textArtefactDescription.setText(artefact.getDescription());
        textArtefactWorking.setText(artefact.getWorkingToString());
        if(artefact.getYear()!=-1){
            textArtefactYear.setText(Integer.toString(artefact.getYear()));
        }
        if (artefact.getThumbnail() != null && !artefact.getThumbnail().isEmpty()) {
            Picasso.with(this).load(artefact.getThumbnail()).into(imageArtefactThumbnail);
        } else {
            imageArtefactThumbnail.setImageResource(R.mipmap.ic_launcher);
        }
    }
}
