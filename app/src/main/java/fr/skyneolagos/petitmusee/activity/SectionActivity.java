package fr.skyneolagos.petitmusee.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.List;

import fr.skyneolagos.petitmusee.R;
import fr.skyneolagos.petitmusee.service.Adapter.ArtefactSectionAdapter;
import fr.skyneolagos.petitmusee.service.section.Section;

public class SectionActivity extends AppCompatActivity {
    private ArtefactSectionAdapter artefactSectionAdapter;
    private RecyclerView recyclerViewSection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section);

        List<Section> sectionList = MainActivity.sectionList;

        recyclerViewSection = findViewById(R.id.recyclerViewSection);
        artefactSectionAdapter = new ArtefactSectionAdapter(sectionList, this);
        recyclerViewSection.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerViewSection.setAdapter(artefactSectionAdapter);
        recyclerViewSection.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));


    }
}
